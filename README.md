# tdz-shared

Shared types and libs for the tdz projects.

## Usage

Add as a dependency to a referencing project:

```bash
npm install https://gitlab.com/theater-der-zeit/tdz-shared.git
```

Always reference compiled code from the `lib` folder, NOT from the `src` folder,
in dependent projects.

## Development

Add or edit code in the `src` folder. Then run the build script `npm run build`.
This compiles the typescript code to the `lib` folder.

Commit your changes, including the freshly compiled `lib` folder.

In the referencing project, run `npm upgrade tdz-shared` to apply the changes.

## License

Copyright 2020 Thomas Gorny

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
