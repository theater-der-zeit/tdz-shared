module.exports = {
	preset: 'ts-jest',
	testEnvironment: 'node',
	prettierPath: null, // temporary bugfix until jest 30.0 supports prettier 3.0
}
