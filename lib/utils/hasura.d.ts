type OmitKeys = {
    [key: string]: true | OmitKeys;
};
interface InsertTransformOptions {
    /**
     * can specify nested keys that won't be wrapped
     */
    omitKeys?: OmitKeys;
}
/**
 * This function wraps all nested object into {data: obj} format to implement the hasura insert input convention.
 *
 * @param model object that should be transformed to hasura input
 * @param options
 */
export declare function dataToHasuraInsertInput(model: any, { omitKeys }?: InsertTransformOptions): any;
export {};
