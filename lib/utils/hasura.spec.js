"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var hasura_1 = require("./hasura");
describe('hasura-crud-helpers', function () {
    describe('modelToHasuraInsertInput', function () {
        it('converts nested models to insert inputs', function () {
            var model = {
                foo1: 1,
                fooNull: null,
                Undefined: undefined,
                foo: {
                    bar2: 2,
                    barNull: null,
                    Undefined: undefined,
                    bar: {
                        baz3: 3,
                        bazNull: null,
                        Undefined: undefined,
                        baz: 'baz',
                    },
                },
            };
            expect((0, hasura_1.dataToHasuraInsertInput)(model)).toMatchInlineSnapshot({
                foo: { data: { bar: { data: { baz: 'baz' } } } },
            }, "\n\t\t\t{\n\t\t\t  \"foo\": {\n\t\t\t    \"data\": {\n\t\t\t      \"bar\": {\n\t\t\t        \"data\": {\n\t\t\t          \"baz\": \"baz\",\n\t\t\t          \"baz3\": 3,\n\t\t\t          \"bazNull\": null,\n\t\t\t        },\n\t\t\t      },\n\t\t\t      \"bar2\": 2,\n\t\t\t      \"barNull\": null,\n\t\t\t    },\n\t\t\t  },\n\t\t\t  \"foo1\": 1,\n\t\t\t  \"fooNull\": null,\n\t\t\t}\n\t\t");
        });
        it('can omit keys', function () {
            var model = {
                foo: {
                    bar: {
                        baz: 'baz',
                    },
                },
            };
            expect((0, hasura_1.dataToHasuraInsertInput)(model, { omitKeys: { foo: true } })).toEqual({
                foo: { bar: { baz: 'baz' } },
            });
            expect((0, hasura_1.dataToHasuraInsertInput)(model, { omitKeys: { foo: { bar: true } } })).toEqual({
                foo: { data: { bar: { baz: 'baz' } } },
            });
        });
        it('processes arrays', function () {
            var model = { arr: [{ foo: [{ bar: 'bar' }] }] };
            expect((0, hasura_1.dataToHasuraInsertInput)(model)).toEqual({
                arr: {
                    data: [{ foo: { data: [{ bar: 'bar' }] } }],
                },
            });
            expect((0, hasura_1.dataToHasuraInsertInput)(model, { omitKeys: { arr: { foo: true } } })).toEqual({
                arr: {
                    data: [{ foo: [{ bar: 'bar' }] }],
                },
            });
        });
        it('removes empty array', function () {
            var model = { foo: 1, arr: [] };
            expect((0, hasura_1.dataToHasuraInsertInput)(model)).toEqual({ foo: 1 });
        });
    });
});
