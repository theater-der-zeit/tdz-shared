"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dataToHasuraInsertInput = void 0;
/**
 * This function wraps all nested object into {data: obj} format to implement the hasura insert input convention.
 *
 * @param model object that should be transformed to hasura input
 * @param options
 */
function dataToHasuraInsertInput(model, _a) {
    var _b = _a === void 0 ? {} : _a, _c = _b.omitKeys, omitKeys = _c === void 0 ? {} : _c;
    var input = {};
    var _loop_1 = function (key) {
        var val = model[key];
        if (Array.isArray(val) && !(omitKeys[key] === true)) {
            if (val.length)
                input[key] = {
                    data: val.map(function (v) {
                        return dataToHasuraInsertInput(v, { omitKeys: omitKeys[key] });
                    }),
                };
        }
        else if (val != null &&
            typeof val === 'object' &&
            !(omitKeys[key] === true)) {
            input[key] = {
                data: dataToHasuraInsertInput(val, {
                    omitKeys: omitKeys[key],
                }),
            };
        }
        else if (val !== undefined) {
            input[key] = val;
        }
    };
    for (var key in model) {
        _loop_1(key);
    }
    return input;
}
exports.dataToHasuraInsertInput = dataToHasuraInsertInput;
