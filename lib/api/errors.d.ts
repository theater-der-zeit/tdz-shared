export declare const PublicApiErrors: {
    readonly LoginWrongEmailOrPassword: "LOGIN_WRONG_EMAIL_OR_PASSWORD";
    readonly LoginRateExceeded: "LOGIN_RATE_EXCEEDED";
    readonly LoginBlockedByAdmin: "LOGIN_BLOCKED_BY_ADMIN";
    readonly Login2FARequired: "LOGIN_2FA_REQUIRED";
    readonly RegistrationEmailAlreadyExists: "REGISTRATION_EMAIL_ALREADY_EXISTS";
    readonly RegistrationPasswordTooWeak: "REGISTRATION_PASSWORD_TOO_WEAK";
    readonly SessionTokenInvalid: "SESSION_TOKEN_INVALID";
    readonly OneTimeTokenInvalid: "ONE_TIME_TOKEN_INVALID";
    readonly Unauthenticated: "UNAUTHENTICATED";
    readonly Forbidden: "FORBIDDEN";
    readonly UserInputInvalid: "USER_INPUT_INVALID";
    readonly InternalServerError: "INTERNAL_SERVER_ERROR";
};
export type PublicApiErrorKey = keyof typeof PublicApiErrors;
export type PublicApiErrorCode = typeof PublicApiErrors[PublicApiErrorKey];
