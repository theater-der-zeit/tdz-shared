"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var authentication_1 = require("./authentication");
describe('api authentication', function () {
    describe('isValidPasswordSecurity', function () {
        it('rejects insufficient passwort security', function () {
            expect((0, authentication_1.isValidPasswordSecurity)(authentication_1.PasswordSecurity.INSUFFICIENT)).toBe(false);
            expect((0, authentication_1.isValidPasswordSecurity)(authentication_1.PasswordSecurity.STRONG)).toBe(true);
            expect((0, authentication_1.isValidPasswordSecurity)(authentication_1.PasswordSecurity.SUFFICIENT)).toBe(true);
            expect((0, authentication_1.isValidPasswordSecurity)(authentication_1.PasswordSecurity.WEAK)).toBe(true);
        });
    });
    describe('mapZXCVBNScoreToPasswordSecurity', function () {
        it('mapx zxcvbn score to password security', function () {
            expect((0, authentication_1.mapZXCVBNScoreToPasswordSecurity)(0)).toEqual(authentication_1.PasswordSecurity.INSUFFICIENT);
            expect((0, authentication_1.mapZXCVBNScoreToPasswordSecurity)(1)).toEqual(authentication_1.PasswordSecurity.INSUFFICIENT);
            expect((0, authentication_1.mapZXCVBNScoreToPasswordSecurity)(2)).toEqual(authentication_1.PasswordSecurity.WEAK);
            expect((0, authentication_1.mapZXCVBNScoreToPasswordSecurity)(3)).toEqual(authentication_1.PasswordSecurity.SUFFICIENT);
            expect((0, authentication_1.mapZXCVBNScoreToPasswordSecurity)(4)).toEqual(authentication_1.PasswordSecurity.STRONG);
            expect((0, authentication_1.mapZXCVBNScoreToPasswordSecurity)(-1)).toEqual(authentication_1.PasswordSecurity.INSUFFICIENT);
            expect((0, authentication_1.mapZXCVBNScoreToPasswordSecurity)(5)).toEqual(authentication_1.PasswordSecurity.INSUFFICIENT);
        });
    });
});
