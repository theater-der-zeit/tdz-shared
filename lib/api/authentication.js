"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mapZXCVBNScoreToPasswordSecurity = exports.isValidPasswordSecurity = exports.PasswordSecurity = void 0;
var PasswordSecurity;
(function (PasswordSecurity) {
    PasswordSecurity["INSUFFICIENT"] = "INSUFFICIENT";
    PasswordSecurity["WEAK"] = "WEAK";
    PasswordSecurity["SUFFICIENT"] = "SUFFICIENT";
    PasswordSecurity["STRONG"] = "STRONG";
})(PasswordSecurity || (exports.PasswordSecurity = PasswordSecurity = {}));
function isValidPasswordSecurity(security) {
    return security !== PasswordSecurity.INSUFFICIENT;
}
exports.isValidPasswordSecurity = isValidPasswordSecurity;
function mapZXCVBNScoreToPasswordSecurity(score) {
    switch (score) {
        case 2:
            return PasswordSecurity.WEAK;
        case 3:
            return PasswordSecurity.SUFFICIENT;
        case 4:
            return PasswordSecurity.STRONG;
        default:
            return PasswordSecurity.INSUFFICIENT;
    }
}
exports.mapZXCVBNScoreToPasswordSecurity = mapZXCVBNScoreToPasswordSecurity;
