export declare enum PasswordSecurity {
    INSUFFICIENT = "INSUFFICIENT",
    WEAK = "WEAK",
    SUFFICIENT = "SUFFICIENT",
    STRONG = "STRONG"
}
export declare function isValidPasswordSecurity(security: PasswordSecurity): boolean;
export declare function mapZXCVBNScoreToPasswordSecurity(score: number): PasswordSecurity;
