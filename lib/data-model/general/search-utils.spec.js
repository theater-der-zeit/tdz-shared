"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var search_utils_1 = require("./search-utils");
describe('search-utils', function () {
    describe('processSearchString', function () {
        it('transforms strings to searchQueries', function () {
            expect((0, search_utils_1.processSearchString)('str')).toEqual(['str']);
            expect((0, search_utils_1.processSearchString)('str1 str2')).toEqual([
                "'str1 str2'",
                'str1 & str2',
            ]);
        });
    });
    describe('concatUniqueSearchResults', function () {
        it('concats search result lists, removing duplicates', function () {
            var searchResults1 = [
                { entityId: '1' },
                { entityId: '3' },
                { entityId: '4' },
            ];
            var searchResults2 = [
                { entityId: '2' },
                { entityId: '5' },
                { entityId: '3' },
                { entityId: '6' },
                { entityId: '4' },
            ];
            expect((0, search_utils_1.concatUniqueSearchResults)(searchResults1, searchResults2)).toEqual([
                { entityId: '1' },
                { entityId: '3' },
                { entityId: '4' },
                { entityId: '2' },
                { entityId: '5' },
                { entityId: '6' },
            ]);
        });
        it('allows null or undefined values', function () {
            expect((0, search_utils_1.concatUniqueSearchResults)()).toEqual([]);
            expect((0, search_utils_1.concatUniqueSearchResults)(null, undefined)).toEqual([]);
            expect((0, search_utils_1.concatUniqueSearchResults)(null, [{ entityId: '6' }], undefined)).toEqual([{ entityId: '6' }]);
        });
    });
});
