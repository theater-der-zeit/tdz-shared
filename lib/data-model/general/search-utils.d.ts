export type SearchResultsByType = {
    [key: string]: string[];
};
export interface SearchResultData {
    entityId?: string | null | undefined;
    entityType?: string | null | undefined;
    updatedAt?: string | null | undefined;
}
export declare function searchResultsByType(results: SearchResultData[]): SearchResultsByType;
export declare function processSearchString(s: string): string[];
export declare function concatUniqueSearchResults(...args: Array<SearchResultData[] | null | undefined>): SearchResultData[];
