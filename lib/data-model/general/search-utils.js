"use strict";
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.concatUniqueSearchResults = exports.processSearchString = exports.searchResultsByType = void 0;
function searchResultsByType(results) {
    var searchByType = {};
    results.forEach(function (e) {
        if (e.entityId && e.entityType) {
            if (searchByType[e.entityType]) {
                searchByType[e.entityType].push(e.entityId);
            }
            else {
                searchByType[e.entityType] = [e.entityId];
            }
        }
    });
    return searchByType;
}
exports.searchResultsByType = searchResultsByType;
function processSearchString(s) {
    var words = s.split(/\s/);
    if (words.length > 1) {
        return ["'".concat(s, "'"), words.join(' & ')];
    }
    return [s];
}
exports.processSearchString = processSearchString;
function concatUniqueSearchResults() {
    var e_1, _a, e_2, _b;
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    var idSet = new Set();
    var resultById = {};
    try {
        for (var args_1 = __values(args), args_1_1 = args_1.next(); !args_1_1.done; args_1_1 = args_1.next()) {
            var results = args_1_1.value;
            if (results) {
                try {
                    for (var results_1 = (e_2 = void 0, __values(results)), results_1_1 = results_1.next(); !results_1_1.done; results_1_1 = results_1.next()) {
                        var result = results_1_1.value;
                        if (result.entityId) {
                            idSet.add(result.entityId);
                            resultById[result.entityId] = result;
                        }
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (results_1_1 && !results_1_1.done && (_b = results_1.return)) _b.call(results_1);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
            }
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (args_1_1 && !args_1_1.done && (_a = args_1.return)) _a.call(args_1);
        }
        finally { if (e_1) throw e_1.error; }
    }
    return __spreadArray([], __read(idSet), false).map(function (id) { return resultById[id]; });
}
exports.concatUniqueSearchResults = concatUniqueSearchResults;
