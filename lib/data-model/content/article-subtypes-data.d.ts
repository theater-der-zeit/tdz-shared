export declare const contentArticleSubtypes: {
    readonly NEWS: {
        readonly PRESS_RELEASE: "PRESS_RELEASE";
    };
};
export declare const articleSubtypeData: readonly [{
    readonly identifier: "PRESS_RELEASE";
    readonly type: "NEWS";
    readonly name: {
        readonly de: "Pressemitteilung";
        readonly en: "press release";
    };
}];
