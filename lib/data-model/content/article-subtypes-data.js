"use strict";
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.articleSubtypeData = exports.contentArticleSubtypes = void 0;
// Sync with API article schema!!
var articleTypes = {
    NEWS: 'NEWS',
    ARTICLE: 'ARTICLE',
};
exports.contentArticleSubtypes = (_a = {},
    _a[articleTypes.NEWS] = {
        PRESS_RELEASE: 'PRESS_RELEASE',
    },
    _a);
exports.articleSubtypeData = [
    {
        identifier: exports.contentArticleSubtypes[articleTypes.NEWS].PRESS_RELEASE,
        type: articleTypes.NEWS,
        name: {
            de: 'Pressemitteilung',
            en: 'press release',
        },
    },
];
