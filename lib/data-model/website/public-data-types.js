"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.initialValues = exports.websitePublicDataIdentifiers = void 0;
exports.websitePublicDataIdentifiers = {
    TRANSLATIONS_DE: 'TRANSLATIONS_DE',
    TRANSLATIONS_EN: 'TRANSLATIONS_EN',
};
exports.initialValues = {
    TRANSLATIONS_DE: {},
    TRANSLATIONS_EN: {},
};
