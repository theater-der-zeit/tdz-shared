export declare const websitePublicDataIdentifiers: {
    readonly TRANSLATIONS_DE: "TRANSLATIONS_DE";
    readonly TRANSLATIONS_EN: "TRANSLATIONS_EN";
};
export type WebsitePublicDataIdentifier = keyof typeof websitePublicDataIdentifiers;
export declare const initialValues: {
    [id in WebsitePublicDataIdentifier]?: any;
};
