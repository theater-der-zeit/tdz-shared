// Sync with API article schema!!
const articleTypes = {
	NEWS: 'NEWS',
	ARTICLE: 'ARTICLE',
} as const

export const contentArticleSubtypes = {
	[articleTypes.NEWS]: {
		PRESS_RELEASE: 'PRESS_RELEASE',
	},
} as const

export const articleSubtypeData = [
	{
		identifier: contentArticleSubtypes[articleTypes.NEWS].PRESS_RELEASE,
		type: articleTypes.NEWS,
		name: {
			de: 'Pressemitteilung',
			en: 'press release',
		},
	},
] as const
