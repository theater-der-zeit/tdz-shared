export const websitePublicDataIdentifiers = {
	TRANSLATIONS_DE: 'TRANSLATIONS_DE',
	TRANSLATIONS_EN: 'TRANSLATIONS_EN',
} as const

export type WebsitePublicDataIdentifier = keyof typeof websitePublicDataIdentifiers

export const initialValues: { [id in WebsitePublicDataIdentifier]?: any } = {
	TRANSLATIONS_DE: {},
	TRANSLATIONS_EN: {},
}
