import {
	concatUniqueSearchResults,
	processSearchString,
	SearchResultData,
} from './search-utils'

describe('search-utils', () => {
	describe('processSearchString', () => {
		it('transforms strings to searchQueries', () => {
			expect(processSearchString('str')).toEqual(['str'])
			expect(processSearchString('str1 str2')).toEqual([
				"'str1 str2'",
				'str1 & str2',
			])
		})
	})

	describe('concatUniqueSearchResults', () => {
		it('concats search result lists, removing duplicates', () => {
			const searchResults1: SearchResultData[] = [
				{ entityId: '1' } as SearchResultData,
				{ entityId: '3' } as SearchResultData,
				{ entityId: '4' } as SearchResultData,
			]
			const searchResults2: SearchResultData[] = [
				{ entityId: '2' } as SearchResultData,
				{ entityId: '5' } as SearchResultData,
				{ entityId: '3' } as SearchResultData,
				{ entityId: '6' } as SearchResultData,
				{ entityId: '4' } as SearchResultData,
			]

			expect(
				concatUniqueSearchResults(searchResults1, searchResults2),
			).toEqual([
				{ entityId: '1' },
				{ entityId: '3' },
				{ entityId: '4' },
				{ entityId: '2' },
				{ entityId: '5' },
				{ entityId: '6' },
			])
		})

		it('allows null or undefined values', () => {
			expect(concatUniqueSearchResults()).toEqual([])
			expect(concatUniqueSearchResults(null, undefined)).toEqual([])
			expect(
				concatUniqueSearchResults(
					null,
					[{ entityId: '6' } as SearchResultData],
					undefined,
				),
			).toEqual([{ entityId: '6' }])
		})
	})
})
