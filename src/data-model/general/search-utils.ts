export type SearchResultsByType = {
	[key: string]: string[]
}

export interface SearchResultData {
	entityId?: string | null | undefined
	entityType?: string | null | undefined
	updatedAt?: string | null | undefined
}

export function searchResultsByType(results: SearchResultData[]) {
	const searchByType = {} as SearchResultsByType
	results.forEach(e => {
		if (e.entityId && e.entityType) {
			if (searchByType[e.entityType]) {
				searchByType[e.entityType]!.push(e.entityId)
			} else {
				searchByType[e.entityType] = [e.entityId]
			}
		}
	})
	return searchByType
}

export function processSearchString(s: string) {
	const words = s.split(/\s/)
	if (words.length > 1) {
		return [`'${s}'`, words.join(' & ')]
	}
	return [s]
}

export function concatUniqueSearchResults(
	...args: Array<SearchResultData[] | null | undefined>
) {
	const idSet = new Set<string>()
	const resultById: { [id: string]: SearchResultData } = {}
	for (const results of args) {
		if (results) {
			for (const result of results) {
				if (result.entityId) {
					idSet.add(result.entityId)
					resultById[result.entityId] = result
				}
			}
		}
	}
	return [...idSet].map(id => resultById[id])
}
