export enum PasswordSecurity {
	INSUFFICIENT = 'INSUFFICIENT',
	WEAK = 'WEAK',
	SUFFICIENT = 'SUFFICIENT',
	STRONG = 'STRONG',
}

export function isValidPasswordSecurity(security: PasswordSecurity): boolean {
	return security !== PasswordSecurity.INSUFFICIENT
}

export function mapZXCVBNScoreToPasswordSecurity(
	score: number,
): PasswordSecurity {
	switch (score) {
		case 2:
			return PasswordSecurity.WEAK
		case 3:
			return PasswordSecurity.SUFFICIENT
		case 4:
			return PasswordSecurity.STRONG
		default:
			return PasswordSecurity.INSUFFICIENT
	}
}
