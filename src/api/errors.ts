export const PublicApiErrors = {
	LoginWrongEmailOrPassword: 'LOGIN_WRONG_EMAIL_OR_PASSWORD',
	LoginRateExceeded: 'LOGIN_RATE_EXCEEDED',
	LoginBlockedByAdmin: 'LOGIN_BLOCKED_BY_ADMIN',
	Login2FARequired: 'LOGIN_2FA_REQUIRED',

	RegistrationEmailAlreadyExists: 'REGISTRATION_EMAIL_ALREADY_EXISTS',
	RegistrationPasswordTooWeak: 'REGISTRATION_PASSWORD_TOO_WEAK',

	SessionTokenInvalid: 'SESSION_TOKEN_INVALID',
	OneTimeTokenInvalid: 'ONE_TIME_TOKEN_INVALID',

	Unauthenticated: 'UNAUTHENTICATED',
	Forbidden: 'FORBIDDEN',

	UserInputInvalid: 'USER_INPUT_INVALID',
	InternalServerError: 'INTERNAL_SERVER_ERROR',
} as const

export type PublicApiErrorKey = keyof typeof PublicApiErrors

export type PublicApiErrorCode = typeof PublicApiErrors[PublicApiErrorKey]
