import {
	isValidPasswordSecurity,
	mapZXCVBNScoreToPasswordSecurity,
	PasswordSecurity,
} from './authentication'

describe('api authentication', () => {
	describe('isValidPasswordSecurity', () => {
		it('rejects insufficient passwort security', () => {
			expect(isValidPasswordSecurity(PasswordSecurity.INSUFFICIENT)).toBe(false)
			expect(isValidPasswordSecurity(PasswordSecurity.STRONG)).toBe(true)
			expect(isValidPasswordSecurity(PasswordSecurity.SUFFICIENT)).toBe(true)
			expect(isValidPasswordSecurity(PasswordSecurity.WEAK)).toBe(true)
		})
	})

	describe('mapZXCVBNScoreToPasswordSecurity', () => {
		it('mapx zxcvbn score to password security', () => {
			expect(mapZXCVBNScoreToPasswordSecurity(0)).toEqual(
				PasswordSecurity.INSUFFICIENT,
			)
			expect(mapZXCVBNScoreToPasswordSecurity(1)).toEqual(
				PasswordSecurity.INSUFFICIENT,
			)
			expect(mapZXCVBNScoreToPasswordSecurity(2)).toEqual(PasswordSecurity.WEAK)
			expect(mapZXCVBNScoreToPasswordSecurity(3)).toEqual(
				PasswordSecurity.SUFFICIENT,
			)
			expect(mapZXCVBNScoreToPasswordSecurity(4)).toEqual(
				PasswordSecurity.STRONG,
			)
			expect(mapZXCVBNScoreToPasswordSecurity(-1)).toEqual(
				PasswordSecurity.INSUFFICIENT,
			)
			expect(mapZXCVBNScoreToPasswordSecurity(5)).toEqual(
				PasswordSecurity.INSUFFICIENT,
			)
		})
	})
})
