type OmitKeys = { [key: string]: true | OmitKeys }
interface InsertTransformOptions {
	/**
	 * can specify nested keys that won't be wrapped
	 */
	omitKeys?: OmitKeys
}

/**
 * This function wraps all nested object into {data: obj} format to implement the hasura insert input convention.
 *
 * @param model object that should be transformed to hasura input
 * @param options
 */
export function dataToHasuraInsertInput(
	model: any,
	{ omitKeys = {} }: InsertTransformOptions = {},
) {
	const input: any = {}

	for (const key in model) {
		const val = model[key]
		if (Array.isArray(val) && !(omitKeys[key] === true)) {
			if (val.length)
				input[key] = {
					data: val.map(v =>
						dataToHasuraInsertInput(v, { omitKeys: omitKeys[key] as OmitKeys }),
					),
				}
		} else if (
			val != null &&
			typeof val === 'object' &&
			!(omitKeys[key] === true)
		) {
			input[key] = {
				data: dataToHasuraInsertInput(val, {
					omitKeys: omitKeys[key] as OmitKeys,
				}),
			}
		} else if (val !== undefined) {
			input[key] = val
		}
	}

	return input
}
