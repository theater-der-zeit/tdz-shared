import { dataToHasuraInsertInput } from './hasura'

describe('hasura-crud-helpers', () => {
	describe('modelToHasuraInsertInput', () => {
		it('converts nested models to insert inputs', () => {
			const model = {
				foo1: 1,
				fooNull: null,
				Undefined: undefined,
				foo: {
					bar2: 2,
					barNull: null,
					Undefined: undefined,
					bar: {
						baz3: 3,
						bazNull: null,
						Undefined: undefined,
						baz: 'baz',
					},
				},
			}
			expect(dataToHasuraInsertInput(model)).toMatchInlineSnapshot(
				{
					foo: { data: { bar: { data: { baz: 'baz' } } } },
				},
				`
			{
			  "foo": {
			    "data": {
			      "bar": {
			        "data": {
			          "baz": "baz",
			          "baz3": 3,
			          "bazNull": null,
			        },
			      },
			      "bar2": 2,
			      "barNull": null,
			    },
			  },
			  "foo1": 1,
			  "fooNull": null,
			}
		`,
			)
		})

		it('can omit keys', () => {
			const model = {
				foo: {
					bar: {
						baz: 'baz',
					},
				},
			}

			expect(
				dataToHasuraInsertInput(model, { omitKeys: { foo: true } }),
			).toEqual({
				foo: { bar: { baz: 'baz' } },
			})

			expect(
				dataToHasuraInsertInput(model, { omitKeys: { foo: { bar: true } } }),
			).toEqual({
				foo: { data: { bar: { baz: 'baz' } } },
			})
		})

		it('processes arrays', () => {
			const model = { arr: [{ foo: [{ bar: 'bar' }] }] }

			expect(dataToHasuraInsertInput(model)).toEqual({
				arr: {
					data: [{ foo: { data: [{ bar: 'bar' }] } }],
				},
			})

			expect(
				dataToHasuraInsertInput(model, { omitKeys: { arr: { foo: true } } }),
			).toEqual({
				arr: {
					data: [{ foo: [{ bar: 'bar' }] }],
				},
			})
		})

		it('removes empty array', () => {
			const model = { foo: 1, arr: [] }
			expect(dataToHasuraInsertInput(model)).toEqual({ foo: 1 })
		})
	})
})
